#ifndef __PXGBASE_H
#define __PXGBASE_H
#endif

#if defined(__unix__) || defined(__unix) || \
        (defined(__APPLE__) && defined(__MACH__))
#define UNIX_OR_OSX
#else
#define WIN32_LEAN_AND_MEAN
#endif
