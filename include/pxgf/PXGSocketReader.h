#ifndef __PXGSOCKET_READER_H
#define __PXGSOCKET_READER_H
// Copyright 2006 Peralex Electronics (Pty) Ltd
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#include "PXGBase.h"

#ifdef UNIX_OR_OSX // unix or OSX
#include <cstdio>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <netdb.h>
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#else // WIN32
#define WIN32_LEAN_AND_MEAN
#include <winsock2.h>
#endif

#include "PXGReaderBase.h"
#include "PXGStreamWriter.h"
#include "ResetHandler.h"

namespace pxgf
{
	/// PXG Reader class that accepts an input PXGF stream from a socket
	class cPXGSocketReader : public cPXGReaderBase
	{
	public:
		/// Constructor
		cPXGSocketReader();
#ifdef UNIX_OR_OSX
		/// Constructor
		/// @param hSocket Socket handle for the socket that will be read from.
		cPXGSocketReader(int hSocket);
#else
		/// Constructor
		/// @param hSocket Socket handle for the socket that will be read from.
		cPXGSocketReader(SOCKET hSocket);
#endif

		/// Process an input stream
		//
		/// Calling this stream instructs the engine to start processing the given
		/// stream from the specfied socket and assuming the endian
		/// ordering specified. It will continuously process chunks until the socket
		/// connection is broken, or you specify that you wish it to
		/// stop by calling removeFromStream().(This processStream is deprecated
		/// and the other one which specfies endian by the eEndianType enum should
		/// be used instead).
		/// @param hSocket A valid socket to read the data stream from
		/// @param bBigEndian Is the stream big-endian byte order?
#ifdef UNIX_OR_OSX
		void processStream(int hSocket, bool bBigEndian);
#else
		void processStream(SOCKET hSocket, bool bBigEndian);
#endif

		/// Process an input stream
		//
		/// This is an alternative function to the previous processStream function
		/// that allows specifying that the endian should be automatically
		/// detected.
		/// @param hSocket A valid socket to read the data stream from
		/// @param EndianType Specify the endian type of the input stream. Use Endian_Auto
		/// for it to be automatically detected.
#ifdef UNIX_OR_OSX
		void processStream(int hSocket, eEndianType EndianType);
#else
		void processStream(SOCKET hSocket, eEndianType EndianType);
#endif

	protected:
		/// Perform a blocking read of the desired length and throw eof exception
		/// @param pData Destination for read data
		/// @param iLength Number of bytes to read
		virtual void blockingRead(char *pData, int iLength) override;

	private:
#ifdef UNIX_OR_OSX
		int m_hSocket; ///< Handle to the system socket
#else //unix
		SOCKET m_hSocket; ///< Handle to the system socket
#endif
	};
}

#endif
